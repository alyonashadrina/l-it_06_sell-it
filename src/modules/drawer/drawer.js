import React, { Component } from 'react';
import logoWhiteMd from '../../assets/logo-white--md.png';
import SignInForm from './SignInForm';
import SignUpForm from './SignUpForm';

class Drawer extends Component {
  state = {
    opened: this.props.opened,
    // logMode: this.props.logMode,
    logMode: "Sign In"

  }

  changeLogModeToSignIn() {
    this.setState({
      logMode: "Sign In"
    })
  }

  changeLogModeToSignUp() {
    this.setState({
      logMode: "Sign Up"
    })
  }

  render() {
    return (
      <div className={"side-panel " + (this.state.opened ? "side-panel--opened" : "")}>
        <img className="side-panel__logo" src={logoWhiteMd}/>
        <div className="side-panel__mode">
          <label className={"btn btn--white side-panel__mode-btn " + (this.state.logMode === "Sign In" ? "side-panel__mode-btn--active" : "") } onClick={this.changeLogModeToSignIn.bind(this)}>
            <input type="radio" name="mode" value="Sign In" checked="checked"/>Sign In
          </label>
          <label className={"btn btn--white side-panel__mode-btn " + (this.state.logMode === "Sign Up" ? "side-panel__mode-btn--active" : "") } onClick={this.changeLogModeToSignUp.bind(this)}>
            <input type="radio" name="mode" value="Sign Up"/>Sign Up
          </label>
        </div>
        <div className="side-panel__form">
          { this.state.logMode === "Sign In" ? <SignInForm modeName={this.state.logMode}/> : <SignUpForm modeName={this.state.logMode}/> }
        </div>
      </div>
    )

  }
}

export default Drawer;
