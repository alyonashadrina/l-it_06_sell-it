import React, { Component } from 'react';

class SignUpForm extends Component {
  render() {
    return (
      <form className="log-form">
        <label className="log-form__field">
          <input className="log-form__input" type="text" required="required" placeholder=" "/><span className="log-form__text">Name</span>
        </label>
        <label className="log-form__field">
          <input className="log-form__input" type="email" required="required" placeholder=" "/><span className="log-form__text">Email</span>
        </label>
        <label className="log-form__field">
          <input className="log-form__input" type="password" required="required" placeholder=" "/><span className="log-form__text">Password</span>
        </label>
        <label className="log-form__field">
          <input className="log-form__input" type="password" required="required" placeholder=" "/><span className="log-form__text">Repeat Password</span>
        </label>
        <button className="btn btn--white btn--full log-form__submit">{this.props.modeName}</button>
      </form>
    )
  }
}

export default SignUpForm;
