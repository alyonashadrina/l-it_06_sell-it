import React, { Component } from 'react';

class ProductCard extends Component {
  render() {
    return(
      <div className="product-card">
        <a href={this.props.link}>
          <img className="product-card__img" src={this.props.img}/>
          <p className="product-card__title">{this.props.title}</p>
        </a>
      </div>
    )
  }
}

export default ProductCard
