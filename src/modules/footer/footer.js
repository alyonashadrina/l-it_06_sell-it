import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <p>
          2017 - front-end labs Light IT
        </p>
      </footer>
    )
  }

}

export default Footer;
