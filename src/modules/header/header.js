import React, { Component } from 'react';
import HeaderLogo from './headerLogo';
import HeaderSearch from './headerSearch';
import HeaderAccountLogged from './headerAccountLogged';
import HeaderAccountNotLogged from './headerAccountNotLogged';

class Header extends Component {
  state = {
    logged: this.props.logged,
  };

  render() {
    return (
      <header className="header">
        <HeaderLogo toggleFunc={this.props.toggleFunc}/>
        <HeaderSearch />
        {this.props.logged === 'logged' ? <HeaderAccountLogged avatar={this.props.avatar} name={this.props.name} logOutFunc={this.props.logOutFunc}/> : <HeaderAccountNotLogged changeLogMode={this.props.changeLogMode}/>}
      </header>
    )
  }

}

export default Header;
