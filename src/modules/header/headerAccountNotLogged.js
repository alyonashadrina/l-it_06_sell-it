import React, { Component } from 'react';
import iconAccount from '../../assets/icon-account.svg';

class HeaderAccountNotLogged extends Component {

  render() {
    return (
      <div className="header__account header__account--not-logged">
        <div className="header__account-wrapper">
          <p className="header__account-profile">
            Welcome,
            <a className="header__account-link" href="#"> login </a>
            or
            <a className="header__account-link" href="#"> register </a>
            for start!
          </p>
          <img className="header__account-icon" src={iconAccount} />
        </div>
      </div>
    )
  }
}

export default HeaderAccountNotLogged;
