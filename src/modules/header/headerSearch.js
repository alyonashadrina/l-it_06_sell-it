import React, { Component } from 'react';
import iconSearch from '../../assets/icon-search.svg';

class HeaderSearch extends Component {
  render() {
    return (
      <div className="header__search">
        <form className="header__search-form">
          <button className="header__search-btn">
            <img src={iconSearch}/>
          </button>
          <input className="header__search-input" type="text" />
        </form>
      </div>
    )
  }
}

export default HeaderSearch;
