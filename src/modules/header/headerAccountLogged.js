import React, { Component } from 'react';
import iconAccount from '../../assets/icon-account.svg';

class HeaderAccountLogged extends Component {
  render() {
    return (
      <div className="header__account header__account--logged">
        <div className="header__account-wrapper">
          <a className="header__account-profile" href="#">
            <img className="header__account-avatar" src={this.props.avatar}/>
            <span className="header__account-name">{this.props.name}</span>
          </a>
          <a href="#" onClick={this.props.logOutFunc}>
            <img className="header__account-icon" src={iconAccount}/>
          </a>
        </div>
        <div className="header__account-submenu">
          <a className="btn" href="#"> Add new post </a>
          <a className="btn" href="#"> btn Profile </a>
        </div>
      </div>
    )
  }
}

export default HeaderAccountLogged;
