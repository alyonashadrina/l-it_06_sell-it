import React, { Component } from 'react';
import logoBlueSm from '../../assets/logo-blue--sm.png';

class HeaderLogo extends Component {
  render() {
    return (
      <div className="header__logo">
        <a href="#" onClick={this.props.toggleFunc}>
          <img src={logoBlueSm} alt="Sell it! - logo"/>
        </a>
      </div>
    )
  }
}

export default HeaderLogo;
