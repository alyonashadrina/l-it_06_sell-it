import React, { Component } from 'react';

import './App.scss';

import Header from './modules/header/header';
import Footer from './modules/footer/footer';
import PageLogin from './pages/login/index';
import PageProductList from './pages/productList/index';

import background from './assets/bg-page-login.png';
import avatar from './assets/avatar.png';


class App extends Component {

  products = [{  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'}, {  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'}, {  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'}, {  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'}, {  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'}, {  link: 'https://www.myntra.com/trolley-bag/safari/safari-unisex-black-regloss-antiscratch-large-suitcase/1850670/buy', img: 'http://assets.myntassets.com/assets/images/1850662/2017/5/19/11495186291209-Safari-Unisex-Maroon-Regloss-Antiscratch-Small-Trolley-Suitcase-1591495186290981-1.jpg',  title: 'Unisex Black Regloss-Antiscratch Large Suitcase'},];

  state = {
    account: {
      logMode: "Sign In",
      logged: "logged",
      name: "Name Name rgergerg ergergegergergerg ergergergegeer",
      avatar: avatar
    },
    page: {
      drawerOpened: true,
      name: <PageProductList products={this.products}/>,
      classes: "page-product-list",
    }
  }

  changeLogMode(val){
    this.setState({
      account: { ...this.state.account,
        logMode: val
      }
    });
  }

  changePage() {
    if(this.state.page.classes === "page-login") {
      this.setState({
        page: { ...this.state.page,
          name: <PageProductList products={this.products}/>,
          classes: "page-product-list"
        }
      });
    } else {
      this.setState({
        account: { ...this.state.account,
          logged: "not-logged",
        }
      });
      this.setState({
        page: { ...this.state.page,
          name: <PageLogin logMode="Sign Up" drawerOpened={this.state.logged !== "logged" ? true : false} changeLogMode={this.changeLogMode.bind(this)}/>,
          classes: "page-login"
        }
      });
    }
  }

  logOut() {
    console.log('log out')
    this.setState({
      account: { ...this.state.account,
        logged: "not-logged"
      }
    });
    this.setState({
      page: { ...this.state.page,
        name: <PageLogin logMode="Sign Up" drawerOpened={true} />,
        classes: "page-login"
      }
    })
  }

  render() {
    return (
      <div className={this.state.page.classes}>
        <Header logged={this.state.account.logged} name={this.state.account.name} avatar={this.state.account.avatar} toggleFunc={this.changePage.bind(this)} logOutFunc={this.logOut.bind(this)} />
        {this.state.page.name}
        <Footer />
      </div>
    );
  }
}

export default App;
