import React, { Component } from 'react';
import ProductCard from '../../modules/productCard/productCard';

const PageProductList = ({ products }) => {
  let listOfProducts = products.map((product, i) =>
    <ProductCard link={product.link} img={product.img} title={product.title} key={i}/>
  );
  return (
    <div className="">
      <main className="container-lg page-container">
        <div className="product-container">
          {listOfProducts}
        </div>
      </main>
    </div>
  )
};

export default PageProductList
