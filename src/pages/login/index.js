import React, { Component } from 'react';
import Drawer from '../../modules/drawer/drawer';

const PageLogin = ({ logMode, drawerOpened }) => (
  <div className="page-login">
    <Drawer logMode={logMode} opened={drawerOpened}/>
    <main className="page-container">
    </main>
  </div>
);

export default PageLogin
